use std::f64::consts::PI;

fn main() {
    let a = PI / 4.0;
    let b = PI / 3.0;
    let c = PI / 6.0;

    let x = calculate_x(1.0, 2.0, 3.0, a, b, c);
    println!("x = {}", x);
}

fn calculate_x(i: f64, j: f64, k: f64, a: f64, b: f64, c: f64) -> f64 {
    return (
        j * (a.sin() * b.sin() * c.cos()) -
        k * (a.cos() * b.sin() * c.sin()) +
        j * (a.cos() * c.sin()) +
        k * (a.sin() * c.sin()) +
        i * (b.cos() * c.cos())
    );

    fn calculate_y(i: f64, j: f64, k: f64, a: f64, b: f64, c: f64) -> f64 {
        return j * ();
    }
}
